const Game = require("./game");
const cliSelect = require('cli-select');
const user = require('./model/User');
const chalk = require('chalk');

let notAWinner = false;
let game = new Game();
let value;

game.add(new user("Axel"));
game.add(new user("Bertrand"));
game.add(new user("Yves"));
game.add(new user("Christ"));
game.add(new user("Bryan"));
game.setWinCondition();
game.setTotalPlayer();

console.log("Choisissez un type de question")
cliSelect({
  values: ['Rock', 'Techno'],
  valueRenderer: (value, selected) => {
      if (selected) {
          return chalk.underline(value);
      }
      return value;
  },
}).then(async (type) => {
  game.setPlayableCategory(type.value)
  await main();
});

cliSelect();

async function main() {
  if(game.isPlayable(game.howManyPlayers())) {
    do {
      console.log('---------NEW TURN---------');
      let menu = game.getMenu();
      await menu.then(async (value) => {
        if(value.id == 0){
          notAWinner = await game.rollManagement();
        }
        if (value.id == 1) {
          notAWinner = game.leaveGameManagement();
        }
      });
      } while (notAWinner === true);
  } else {
    console.log("Il n'y a pas le bon nombre de joueur pour participer")
  }
}
