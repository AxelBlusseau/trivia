const prompt = require("prompt-sync")();
const cliSelect = require('cli-select');
const chalk = require('chalk');

var Game = function () {


  let players = [];
  let winners = [];
  let places = new Array(6);
  let purses = new Array(6);
  let inPenaltyBox = new Array(6);
  let popQuestions = [];
  let scienceQuestions = [];
  let sportsQuestions = [];
  let rockQuestions = [];
  let technoQuestions = [];
  var playerBeforeInPenaltyBox = false;
  var imposedNextPlayerTheme = "";
  let totalPlayer;

  let currentPlayer = 0;
  let isGettingOutOfPenaltyBox = false;
  let winConditition = 6;


  this.setTotalPlayer = function(){
    totalPlayer = players.length;
  }

  //Rock or Techno
  var getPlayableCategory = '';
  this.setPlayableCategory = function (category) {
    getPlayableCategory = category
  };

  let didPlayerWin = function () {
    if (purses[currentPlayer] >= winConditition) {
      winners.push(players[currentPlayer])
      console.log(players[currentPlayer].name + " a fini !! ######")
      players.splice(currentPlayer, 1);
      currentPlayer--;
    };

    if(winners.length === 3 || winners.length === totalPlayer){
      let rank = 1;
      console.log("###############")
      console.log("### WINNERS ###")
      winners.map(user => {
        console.log(rank + "# " + user.name)
        rank++
      })
      console.log("###############")
      return false;
    } else {
      return true;
    };
  };

  this.getCategory = () => { return currentCategory(); }

  let currentCategory = function () {
    if ([0, 4, 8].includes(places[currentPlayer]))
      return 'Pop';
    if ([1, 5, 9].includes(places[currentPlayer]))
      return 'Science';
    if ([2, 6, 10].includes(places[currentPlayer]))
      return 'Sports';
    return getPlayableCategory; //Rock or Techno
  };


  for (let i = 0; i < 2; i++) {
    popQuestions.push("Pop Question " + i);
    scienceQuestions.push("Science Question " + i);
    sportsQuestions.push("Sports Question " + i);
    rockQuestions.push("Rock Question " + i);
    technoQuestions.push("Techno Question " + i);
  }

  this.isPlayable = function (howManyPlayers) {
    return howManyPlayers >= 2 && howManyPlayers <= 6;
  };

  this.add = function (player) {
    players.push(player);
    places[this.howManyPlayers() - 1] = 0;
    purses[this.howManyPlayers() - 1] = 0;
    inPenaltyBox[this.howManyPlayers() - 1] = false;

    console.log(player.name + " was added");
    console.log("They are player number " + players.length);

    return true;
  };

  this.howManyPlayers = function () {
    return players.length;
  };


  let askQuestion = function () {
    if (currentCategory() === 'Pop') {
      let removedPopQuestion = popQuestions.shift();
      console.log(removedPopQuestion);
      popQuestions.push(removedPopQuestion)
    }
    if (currentCategory() === 'Science') {
      let removedScienceQuestion = scienceQuestions.shift();
      console.log(removedScienceQuestion);
      scienceQuestions.push(removedScienceQuestion)
    }
    if (currentCategory() === 'Sports') {
      let removedSportsQuestion = sportsQuestions.shift();
      console.log(removedSportsQuestion);
      sportsQuestions.push(removedSportsQuestion)
    }
    if (currentCategory() === 'Rock') {
      let removedRockQuestion = rockQuestions.shift();
      console.log(removedRockQuestion);
      rockQuestions.push(removedRockQuestion)
    }

    if (currentCategory() === 'Techno') {
      let removedTechnoQuestion = technoQuestions.shift();
      console.log(removedTechnoQuestion);
      technoQuestions.push(removedTechnoQuestion)
    }
  };
  console.log(inPenaltyBox);

  this.setPlace = function (roll) {
    places[currentPlayer] = places[currentPlayer] + roll;
    if (places[currentPlayer] > 12) {
      places[currentPlayer] = places[currentPlayer] - 13;
    }
  }

  this.roll = function (roll) {
    console.log(players[currentPlayer].name + " is the current player");
    console.log("They have rolled a " + roll);
    if (inPenaltyBox[currentPlayer]) {
      console.log("vous avez " + ((1 / players[currentPlayer].nbPrison) * 100) + "% de chance de sortir");
      let nbRandom = Math.floor(Math.random() * (players[currentPlayer].nbPrison - 1)) + 1;
      if (nbRandom === 1) {
        isGettingOutOfPenaltyBox = true;

        console.log(players[currentPlayer].name + " is getting out of the penalty box");
        inPenaltyBox[currentPlayer] = false

        this.setPlace(roll);

        console.log(players[currentPlayer].name + "'s new location is " + places[currentPlayer]);
        console.log("The category is " + currentCategory());
        askQuestion();
      } else {
        console.log(players[currentPlayer].name + " is not getting out of the penalty box");
        isGettingOutOfPenaltyBox = false;
      }
    } else {
      places[currentPlayer] = places[currentPlayer] + roll;
      if (places[currentPlayer] > 12) {
        places[currentPlayer] = places[currentPlayer] - 13;
      }
      console.log(players[currentPlayer].name + "'s new location is " + places[currentPlayer]);
      if (playerBeforeInPenaltyBox) {
        console.log("The category is " + imposedNextPlayerTheme);
      } else {
        console.log("The category is " + currentCategory());
        askQuestion();
      }
    }
  };

  this.wasCorrectlyAnswered = function () {
    if (inPenaltyBox[currentPlayer]) {
      if (isGettingOutOfPenaltyBox) {
        console.log('Answer was correct!!!!');
        purses[currentPlayer] += 1;
        console.log(players[currentPlayer].name + " now has " +
          purses[currentPlayer] + " Gold Coins.");

        let winner = didPlayerWin();

        return winner;
      } else {
        return true;
      }


    } else {

      console.log("Answer was correct!!!!");
      if (players[currentPlayer].winstreak === 0) {
        purses[currentPlayer] = 1;
      }
      else {
        console.log("vous êtes en série de " + (players[currentPlayer].winstreak + 1) + " bonne réponse !!");
        purses[currentPlayer] = purses[currentPlayer] + (players[currentPlayer].winstreak + 1);
      }
      players[currentPlayer].winstreak = players[currentPlayer].winstreak + 1;
      console.log(players[currentPlayer].name + " now has " +
        purses[currentPlayer] + " Gold Coins.");

      let winner = didPlayerWin();

      return winner;
    }
  };

  this.wrongAnswer = function () {
    console.log('Question was incorrectly answered');
    players[currentPlayer].winstreak = 0;
    this.chooseNextPlayerQuestion();
    players[currentPlayer].nbPrison = players[currentPlayer].nbPrison + 1
    console.log(players[currentPlayer].name + " was sent to the penalty box");
    inPenaltyBox[currentPlayer] = true;
    return true;
  };

  this.chooseNextPlayerQuestion = function () {
    playerBeforeInPenaltyBox = true;
    let input;
    while(input != "done") {
        input = prompt("Sur quel thème portera la question du prochain joueur ?");
        if (input == "Rock" || input == "Techno" || input == "Science" || input == "Sports" || input == "Pop" ) {
          imposedNextPlayerTheme = input;
          input = "done"
        }
        else console.log("Veuillez choisir un thème valide ! ");
    }
  };
  this.getMenu = function () {
    return createMenu(["Lancer de dés", "Quitter la partie"]);
  }

  this.setWinCondition = function () {
    do {
      winConditionValue = prompt('Condition de victoire : ');
    } while (winConditionValue < 6);
    winConditition = winConditionValue;
  }

  this.changePlayer = function () {
    currentPlayer += 1;
    if (currentPlayer === players.length)
      currentPlayer = 0;
  }

  //Handle Joker Management (Ask Joker only if player has Joker)
  this.jokerManagement = async function (user) {
    if (user.joker == 1) {
      console.log("Voulez-vous utiliser un joker ?");
      return await createMenu(['Oui', 'Non']).then((value) => {
        if (value.id == 0) {
          console.log("Vous venez d'utiliser votre Joker ! ");
          user.joker = 0;
          return true;
        } else return false;
      });
    } else {
      return false;
    }
  }

  // Gère les jets de dé
  this.rollManagement = async function () {

    // Choisis la catégorie
    this.roll(Math.floor(Math.random() * 6) + 1);

    // Intéraction avec le joker
    if (!inPenaltyBox[currentPlayer]) {
      if (await this.jokerManagement(players[currentPlayer])) {
        return true;
      }
    }

    // Jet de dé
    let value;
    if (Math.floor(Math.random() * 10) == 7) {
      value = this.wrongAnswer();
    } else {
      value = this.wasCorrectlyAnswered();
    }
    this.changePlayer();
    return value;
  }

  this.leaveGameManagement = function () {
    players.splice(currentPlayer, 1);
    console.log("Vous avez été retiré du jeu");
    if (players.length === 1) {
      console.log("la partie s'arrête vous ne pouvez pas jouer tout seul");
      process.exit();
    }

    this.changePlayer();

    return true;
  }
};
// Generation de menu avec une liste d'option
function createMenu(options) {
  return cliSelect({
    values: options,
    valueRenderer: (value, selected) => {
      if (selected) {
        return chalk.underline(value);
      }
      return value;
    },
  });
}
module.exports = Game;
