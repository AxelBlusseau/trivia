const Should = require('should');
const Game = require('../game.js');
const Main = require('../main.js');
var assert = require('assert');
const user = require('../model/User');
const expect = require('expect');

let game = new Game();
game.add(new user("Axel"));
game.add(new user("Bertrand"));
game.setPlayableCategory("Rock");

describe("Test probability category question", function () {
  it("should pass", () => {
    let i = 0;
    let user_1 = [];
    let user_2 = [];
    let index = [];
    let category;
    let iterate = 100000;
    while(i < iterate) {
      game.roll(Math.floor(Math.random() * 6) + 1);
      category = game.getCategory();
      if (!index.includes(category))
        index.push(category);

      i % 2 == 0 ? user_1.push(category) : user_2.push(category)
      i++;
    }
    // console.log(Math.round(user_1.filter((value) => value == "Sports").length));
    index.forEach((i) => {
      console.log("Index : " + i);
      let a = Math.round(user_1.filter((value) => value == i).length) / iterate;
      let b = Math.round(user_2.filter((value) => value == i).length) / iterate;
      expect(a).toBeCloseTo(b);
    });
  });

  it("should access game", function () {
    Should(Game).not.equal(undefined);
  });

  /*it("should define the amount of player", function(){
    let game = new Game();
    var input = "Rock";
    var getPlayableCategory = '';

    game.setPlayableCategory(input);

    (getPlayableCategory).should.equal("Rock");
  });*/

  /*it("Launch the dice", function(){
    let game = new Game();
    var isGettingOutOfPenaltyBox;
    game.roll(6);
    assert.strictEqual(isGettingOutOfPenaltyBox, true);
    //(true).should.equal(true);
  });*/

  it("Should be playable", function(){
    let game = new Game();
    assert.strictEqual(game.isPlayable(5), true);
  });

  it("Shouldn't be playable if amount of players < 7", function(){
    let game = new Game();
    assert.strictEqual(game.isPlayable(7), false);
  });

  it("Shouldn't be playable if amount of players > 1", function(){
    let game = new Game();
    assert.strictEqual(game.isPlayable(1), false);
  });
  

});

